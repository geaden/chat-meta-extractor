package com.atlassian.candidate.extractor;

import com.atlassian.candidate.extractor.impl.LinksExtractor;
import com.atlassian.candidate.model.Link;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.IOException;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

/**
 * Unit-tests for {@link LinksExtractor}.
 *
 * @author Gennady Denisov
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(Jsoup.class)
public class LinksExtractorTest {

    private static final String TEST_MESSAGE = "Olympics are starting soon; http://www.nbcolympics.com";

    private static final String TEST_MULTIPLE_LINKS_MESSAGE = "foo https://foo.bz http://bar.br";

    private static final String TEST_TITLE = "NBC Olympics | 2014 NBC Olympics in Sochi Russia";

    @Mock
    private Connection connection;

    @Mock
    private Document document;

    private AbstractMetaExtractor<Link> linksExtractor;

    @Before
    public void initTestCase() throws Exception {
        MockitoAnnotations.initMocks(this);
        PowerMockito.mockStatic(Jsoup.class);
        when(connection.get()).thenReturn(document);
        linksExtractor = new LinksExtractor();
    }

    @Test
    public void shouldExtractLinksFromMessage() throws Exception {
        PowerMockito.when(Jsoup.connect("http://www.nbcolympics.com"))
                .thenReturn(connection);
        when(document.title()).thenReturn(TEST_TITLE);
        List<Link> links = linksExtractor.extract(TEST_MESSAGE);
        assertThat(links.size(), is(equalTo(1)));
        final Link link = links.get(0);
        assertThat(link.getTitle(), is(equalTo(TEST_TITLE)));
        assertThat(link.getUrl(), is(equalTo("http://www.nbcolympics.com")));
    }

    @Test
    public void shouldExtractLinksFromMessage_multipleLinks() throws Exception {
        PowerMockito.when(Jsoup.connect("https://foo.bz"))
                .thenReturn(connection);
        PowerMockito.when(Jsoup.connect("http://bar.br"))
                .thenReturn(connection);
        when(document.title()).thenReturn("foo").thenReturn("bar");
        List<Link> links = linksExtractor.extract(TEST_MULTIPLE_LINKS_MESSAGE);
        assertThat(links.size(), is(equalTo(2)));
        final Link linkFoo = links.get(0);
        assertThat(linkFoo.getTitle(), is(equalTo("foo")));
        assertThat(linkFoo.getUrl(), is(equalTo("https://foo.bz")));
        final Link linkBar = links.get(1);
        assertThat(linkBar.getTitle(), is(equalTo("bar")));
        assertThat(linkBar.getUrl(), is(equalTo("http://bar.br")));
    }

    @Test
    public void shouldExtractLinks_artifactIOException() throws Exception {
        PowerMockito.when(Jsoup.connect("http://www.nbcolympics.com"))
                .thenReturn(connection);
        when(connection.get()).thenThrow(new IOException());
        final List<Link> links = linksExtractor.extract(TEST_MESSAGE);
        assertThat(links.get(0).getTitle(), is(equalTo("")));
    }
}
