package com.atlassian.candidate.extractor;

import com.atlassian.candidate.extractor.impl.MentionsExtractor;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Unit tests for {@link MentionsExtractor}.
 *
 * @author Gennady Denisov
 */
public class MentionsExtractorTest {

    private static final String TEST_MESSAGE = "@chris you around?";

    private static final String TEST_EMPTY_MENTIONS_MESSAGE = "you around?";

    private static final String TEST_MULTIPLE_MENTIONS_MESSAGE = "@chris @bob @john you around?";

    private MentionsExtractor mentionsExtractor;

    @Before
    public void initTestCase() {
        mentionsExtractor = new MentionsExtractor();
    }

    @Test
    public void shouldExtractMentionsFromMessage() {
        List<String> mentions = mentionsExtractor.extract(TEST_MESSAGE);
        assertThat(mentions.size(), is(equalTo(1)));
        assertThat(mentions, is(equalTo(Collections.singletonList("chris"))));
    }

    @Test
    public void shouldExtractMentions_multipleMentions() {
        List<String> mentions = mentionsExtractor.extract(TEST_MULTIPLE_MENTIONS_MESSAGE);
        assertThat(mentions.size(), is(equalTo(3)));
        assertThat(mentions, is(equalTo(Arrays.asList("chris", "bob", "john"))));
    }

    @Test
    public void shouldExtractMentions_noMentions() {
        List<String> mentions = mentionsExtractor.extract(TEST_EMPTY_MENTIONS_MESSAGE);
        assertThat(mentions.size(), is(equalTo(0)));
    }
}
