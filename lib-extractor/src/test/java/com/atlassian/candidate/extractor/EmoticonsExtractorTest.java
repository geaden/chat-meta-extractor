package com.atlassian.candidate.extractor;

import com.atlassian.candidate.extractor.impl.EmoticonsExtractor;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Unit-test for {@link EmoticonsExtractor}.
 *
 * @author Gennady Denisov
 */
public class EmoticonsExtractorTest {

    private static final String TEST_MESSAGE = "Good morning! (megusta) (coffee)";

    private static final String TEST_EMPTY_EMOTICONS_MESSAGE = "Good morning!";

    private AbstractMetaExtractor<String> emoticonExtractor;

    @Before
    public void initTestCase() {
        // Init class under test
        emoticonExtractor = new EmoticonsExtractor();
    }

    @Test
    public void shouldExtractEmoticonsFromMessage() {
        List<String> emoticons = emoticonExtractor.extract(TEST_MESSAGE);
        assertThat(emoticons.size(), is(equalTo(2)));
        assertThat(emoticons, is(equalTo(Arrays.asList("megusta", "coffee"))));
    }

    @Test
    public void shouldExtractEmoticons_manyParenthesis() {
        List<String> emoticons = emoticonExtractor.extract("(((((success)) ((((megusta)");
        assertThat(emoticons.size(), is(equalTo(2)));
        assertThat(emoticons, is(equalTo(Arrays.asList("success", "megusta"))));
    }

    @Test
    public void shouldExtractEmoticons_noEmoticons() {
        List<String> emoticons = emoticonExtractor.extract(TEST_EMPTY_EMOTICONS_MESSAGE);
        assertThat(emoticons.size(), is(equalTo(0)));
    }
}
