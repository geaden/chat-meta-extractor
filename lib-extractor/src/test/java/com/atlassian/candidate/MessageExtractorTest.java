package com.atlassian.candidate;

import com.atlassian.candidate.extractor.AbstractMetaExtractor;
import com.atlassian.candidate.extractor.MessageExtractor;
import com.atlassian.candidate.extractor.impl.EmoticonsExtractor;
import com.atlassian.candidate.extractor.impl.LinksExtractor;
import com.atlassian.candidate.extractor.impl.MentionsExtractor;
import com.atlassian.candidate.model.Link;
import com.atlassian.candidate.testutils.FileUtils;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.skyscreamer.jsonassert.JSONAssert;

import java.util.regex.Pattern;

import static org.mockito.Mockito.when;

/**
 * Unit tests for {@link MessageExtractor}.
 *
 * @author Gennady Denisov
 */
public class MessageExtractorTest {

    private static final String TEST_MESSAGE = "@bob @john (success) "
            + "such a cool feature; https://twitter.com/jdorfman/status/430511497475670016";

    private static final String TEST_MESSAGE_NO_MENTIONS = "(success) (coffee) cool http://foo.bz";

    private static final String TEST_MESSAGE_NO_EMOTICONS = "@bob cool http://foo.bz";

    private static final String TEST_MESSAGE_NO_LINKS = "@bob (success) foo bar";

    private MessageExtractor messageExtractor;

    private MentionsExtractor mentionsExtractor = new MentionsExtractor();

    private EmoticonsExtractor emoticonsExtractor = new EmoticonsExtractor();

    @Mock
    private LinksExtractor linksExtractor;

    @Before
    public void initTestCase() {
        MockitoAnnotations.initMocks(this);
        when(linksExtractor.pattern()).thenCallRealMethod();
    }

    @Test
    public void shouldExtractMessageContent() throws Exception {
        when(linksExtractor.extract(TEST_MESSAGE)).thenCallRealMethod();
        when(linksExtractor.transform("https://twitter.com/jdorfman/status/430511497475670016"))
                .thenReturn(new Link("https://twitter.com/jdorfman/status/430511497475670016",
                        "Twitter / jdorfman: nice @littlebigdetail from ..."));
        messageExtractor = new MessageExtractor(mentionsExtractor,
                emoticonsExtractor, linksExtractor);
        final String result = messageExtractor.extract(TEST_MESSAGE);
        assertJsonResult(result, "message.json", true);
    }

    @Test
    public void shouldExtractMessageContent_noMentions() throws Exception {
        when(linksExtractor.extract(TEST_MESSAGE_NO_MENTIONS)).thenCallRealMethod();
        when(linksExtractor.transform("http://foo.bz"))
                .thenReturn(new Link("http://foo.bz", "foo"));
        messageExtractor = new MessageExtractor(mentionsExtractor,
                emoticonsExtractor, linksExtractor);
        final String result = messageExtractor.extract(TEST_MESSAGE_NO_MENTIONS);
        assertJsonResult(result, "message_no_mentions.json", true);
    }

    @Test
    public void shouldExtractMessageContent_noEmoticons() throws Exception {
        when(linksExtractor.extract(TEST_MESSAGE_NO_EMOTICONS)).thenCallRealMethod();
        when(linksExtractor.transform("http://foo.bz"))
                .thenReturn(new Link("http://foo.bz", "foo"));
        messageExtractor = new MessageExtractor(mentionsExtractor,
                emoticonsExtractor, linksExtractor);
        final String result = messageExtractor.extract(TEST_MESSAGE_NO_EMOTICONS);
        assertJsonResult(result, "message_no_emoticons.json", true);
    }

    @Test
    public void shouldExtractMessageContent_noLinks() throws Exception {
        when(linksExtractor.extract(TEST_MESSAGE_NO_LINKS)).thenCallRealMethod();
        messageExtractor = new MessageExtractor(mentionsExtractor,
                emoticonsExtractor, linksExtractor);
        final String result = messageExtractor.extract(TEST_MESSAGE_NO_LINKS);
        assertJsonResult(result, "message_no_links.json", true);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void shouldExtractMessageContent_unsupportedExtractor() {
        messageExtractor = new MessageExtractor(new DummyExtractor());
        messageExtractor.extract(TEST_MESSAGE);
    }

    @Test
    @Ignore
    public void shouldExtractMessageContent_withoutMock() throws Exception {
        messageExtractor = new MessageExtractor(mentionsExtractor, emoticonsExtractor,
                new LinksExtractor());
        final String result = messageExtractor.extract(TEST_MESSAGE);
        assertJsonResult(result, "message_jsoup.json", false);
    }

    /**
     * Helper to assert json results.
     *
     * @param result       actual json result.
     * @param resourcePath path to a resource containing expected json.
     * @param strict       if assertion should be strict.
     */
    private void assertJsonResult(final String result,
                                  final String resourcePath, boolean strict) throws Exception {
        JSONAssert.assertEquals(FileUtils.readResourceContent(resourcePath),
                result, strict);
    }

    private static class DummyExtractor extends AbstractMetaExtractor<String> {
        @Override
        public Pattern pattern() {
            return Pattern.compile("(.*)");
        }

        @Override
        public String transform(final String match) {
            return match;
        }
    }
}