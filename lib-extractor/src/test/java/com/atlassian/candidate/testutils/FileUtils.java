package com.atlassian.candidate.testutils;

import java.io.InputStream;
import java.util.Scanner;

/**
 * Utility class to operate with files.
 *
 * @author Gennady Denisov
 */
public final class FileUtils {

    private FileUtils() {
        // Prevent instantiation
    }

    /**
     * Reads content of the resource.
     *
     * @param resourcePath resource path.
     * @return content of the resource.
     */
    public static String readResourceContent(final String resourcePath) {
        final InputStream is = FileUtils.class.getClassLoader().getResourceAsStream(resourcePath);
        try (Scanner sc = new Scanner(is)) {
            return sc.useDelimiter("\\A").hasNext() ? sc.next() : "";
        }
    }
}
