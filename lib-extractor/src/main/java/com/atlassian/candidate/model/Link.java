package com.atlassian.candidate.model;

/**
 * POJO representing extracted link.
 *
 * @author Gennady Denisov
 */
public class Link {

    private String url;
    private String title;

    public Link() {
        // default constructor is required for serialization
    }

    public Link(final String url, final String title) {
        this();
        this.url = url;
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public String getTitle() {
        return title;
    }
}
