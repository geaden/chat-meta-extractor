package com.atlassian.candidate.extractor;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Abstract class for extracting message artifacts.
 *
 * @author Gennady Denisov
 */
public abstract class AbstractMetaExtractor<T> implements MetaExtractor<List<T>> {

    /**
     * Returns compiled regex pattern to extract artifact from a message.
     *
     * @return {@link Pattern} for extraction.
     */
    public abstract Pattern pattern();

    /**
     * Transforms artifact match into an artifact type.
     *
     * @param match match to transform to {@link T}.
     * @return instance of {@link T}.
     */
    public abstract T transform(String match);

    @Override
    public List<T> extract(final String message) {
        final Pattern pattern = pattern();
        final Matcher matcher = pattern.matcher(message);
        final List<T> artifacts = new ArrayList<>();
        while (matcher.find()) {
            final T artifact = transform(matcher.group(1));
            artifacts.add(artifact);
        }
        return artifacts;
    }
}
