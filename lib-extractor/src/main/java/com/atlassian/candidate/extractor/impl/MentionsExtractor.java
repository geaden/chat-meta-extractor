package com.atlassian.candidate.extractor.impl;

import com.atlassian.candidate.extractor.AbstractMetaExtractor;

import java.util.regex.Pattern;

/**
 * Implementation of {@link AbstractMetaExtractor} to extract mentions from message.
 *
 * @author Gennady Denisov
 */
public class MentionsExtractor extends AbstractMetaExtractor<String> {

    private static final String MENTIONS_PATTERN = "@(\\w+)";

    @Override
    public Pattern pattern() {
        return Pattern.compile(MENTIONS_PATTERN);
    }

    @Override
    public String transform(final String match) {
        return match;
    }
}
