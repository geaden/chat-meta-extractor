package com.atlassian.candidate.extractor.impl;

import com.atlassian.candidate.extractor.AbstractMetaExtractor;

import java.util.regex.Pattern;

/**
 * Implementation of {@link AbstractMetaExtractor} to extract emoticons from a message.
 *
 * @author Gennady Denisov
 */
public class EmoticonsExtractor extends AbstractMetaExtractor<String> {

    private static final String EMOTICONS_PATTERN = "\\(([a-z0-9]{1,15})\\)";

    @Override
    public Pattern pattern() {
        return Pattern.compile(EMOTICONS_PATTERN);
    }

    @Override
    public String transform(final String match) {
        return match;
    }
}
