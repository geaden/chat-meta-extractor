package com.atlassian.candidate.extractor;

/**
 * Common interface for content extraction.
 *
 * @author Gennady Denisov
 */
public interface MetaExtractor<T> {

    /**
     * Extracts content from a message.
     * <p>
     * This method should not be called in main thread as certain implementation
     * may require network resource.
     *
     * @return extracted content.
     */
    T extract(final String message);
}
