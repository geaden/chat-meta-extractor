package com.atlassian.candidate.extractor.impl;

import com.atlassian.candidate.extractor.AbstractMetaExtractor;
import com.atlassian.candidate.model.Link;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.util.regex.Pattern;

/**
 * Implementation of {@link AbstractMetaExtractor} to extract links from a message.
 *
 * @author Gennady Denisov
 */
public class LinksExtractor extends AbstractMetaExtractor<Link> {

    private static final String LINKS_PATTERN = "(https?://(?:www\\.|(?!www))[^\\s.]+\\.[^\\s]{2,}|www\\.[^\\s]+\\.[^\\s]{2,})";

    @Override
    public Pattern pattern() {
        return Pattern.compile(LINKS_PATTERN);
    }

    @Override
    public Link transform(final String url) {
        try {
            final Document document = Jsoup.connect(url).get();
            final String title = document.title();
            return new Link(url, title);
        } catch (IOException e) {
            return new Link(url, "");
        }
    }
}
