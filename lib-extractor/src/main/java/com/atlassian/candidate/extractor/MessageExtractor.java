package com.atlassian.candidate.extractor;

import com.atlassian.candidate.extractor.impl.EmoticonsExtractor;
import com.atlassian.candidate.extractor.impl.LinksExtractor;
import com.atlassian.candidate.extractor.impl.MentionsExtractor;
import com.atlassian.candidate.model.Link;
import com.google.gson.Gson;

import java.util.List;

/**
 * A class that extract meta information from a message.
 *
 * @author Gennady Denisov
 */
public class MessageExtractor implements MetaExtractor<String> {

    private final MetaExtractor[] extractors;

    public MessageExtractor(MetaExtractor... extractors) {
        this.extractors = extractors;
    }

    /**
     * Holder of general message information.
     */
    public static class MessageInfo {
        List<String> mentions;
        List<String> emoticons;
        List<Link> links;
    }

    @SuppressWarnings("unchecked")
    @Override
    public String extract(final String message) {
        final MessageInfo messageInfo = new MessageInfo();
        for (MetaExtractor extractor : extractors) {
            Object content = extractor.extract(message);
            if (extractor instanceof MentionsExtractor) {
                messageInfo.mentions = (List<String>) content;
            } else if (extractor instanceof EmoticonsExtractor) {
                messageInfo.emoticons = (List<String>) content;
            } else if (extractor instanceof LinksExtractor) {
                messageInfo.links = (List<Link>) content;
            } else {
                throw new UnsupportedOperationException("Unsupported extractor");
            }
        }
        return new Gson().toJson(messageInfo);
    }
}
