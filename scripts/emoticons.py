#!/usr/env/bin python
"""Script to extract emoticons from HipChat page."""
import sys
import urllib
import os
import json
from bs4 import BeautifulSoup

__author__ = 'Gennady DENISOV <denisovgena@gmail.com>'


EMOTICONS_URL = 'https://www.hipchat.com/emoticons'


def extract_emoticons():
    """Method to extract emoticons into json."""
    cache_dir = os.path.join(os.path.expanduser('~'), '.hipchat')
    emoticons_html = os.path.join(cache_dir, 'emoticons.html')
    if not os.path.exists(cache_dir):
        os.mkdir(cache_dir)
        content = urllib.urlopen(EMOTICONS_URL).read()
        # Save read content
        with open(emoticons_html, 'w') as f:
            f.write(content)
        print('Saved to ~/.hipchat/emoticons.html')
    print('Reading emoticons content...')
    with open(emoticons_html, 'r') as f:
        content = f.read()
    soup = BeautifulSoup(content, "html.parser")
    emoticon_blocks = soup.find_all('div', 'emoticon-block')
    emoticons = {}
    for emoticon_block in emoticon_blocks:
        emoticon = emoticon_block.find('div').string
        img_url = emoticon_block.find('img')['src']
        emoticons[emoticon] = img_url
    with open('../app/src/main/assets/emoticons.json', 'w') as f:
        json.dump(emoticons, f, indent=2)
    print('Emoticons saved to ../app/src/main/assets/emoticons.json')


def main():
    """Main entry point."""
    extract_emoticons()
    return 0

if __name__ == '__main__':
    sys.exit(main())
