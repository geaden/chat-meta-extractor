package com.atlassian.candidate.chatmetaextractor.app;

import com.atlassian.candidate.extractor.impl.LinksExtractor;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Dagger module to provide a {@link LinksExtractor}.
 *
 * @author Gennady Denisov
 */
@Module
public class LinksExtractorModule {

    @Provides
    @Singleton
    LinksExtractor provideLinksExtractor() {
        return new LinksExtractor();
    }
}
