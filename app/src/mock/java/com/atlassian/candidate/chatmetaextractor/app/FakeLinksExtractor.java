package com.atlassian.candidate.chatmetaextractor.app;

import com.atlassian.candidate.extractor.impl.LinksExtractor;
import com.atlassian.candidate.model.Link;

import javax.inject.Inject;

/**
 * Fake implementation of {@link LinksExtractor}.
 *
 * @author Gennady Denisov
 */
class FakeLinksExtractor extends LinksExtractor {

    @Inject
    FakeLinksExtractor() {
    }

    @Override
    public Link transform(String url) {
        return new Link(url, "foo");
    }
}
