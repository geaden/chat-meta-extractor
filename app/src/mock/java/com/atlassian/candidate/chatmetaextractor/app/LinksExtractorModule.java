package com.atlassian.candidate.chatmetaextractor.app;

import com.atlassian.candidate.extractor.impl.LinksExtractor;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;

/**
 * Dagger module to provide a {@link LinksExtractor}.
 *
 * @author Gennady Denisov
 */
@Module
abstract class LinksExtractorModule {

    @Binds
    @Singleton
    abstract LinksExtractor provideLinksExtractor(FakeLinksExtractor linksExtractor);
}
