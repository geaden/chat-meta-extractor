package com.atlassian.candidate.chatmetaextractor.app.data.source;

import android.database.Cursor;
import android.support.annotation.NonNull;

import com.atlassian.candidate.chatmetaextractor.app.data.ChatMessage;
import com.atlassian.candidate.chatmetaextractor.app.data.source.local.ChatLocalDataSource;

import javax.inject.Inject;
import javax.inject.Singleton;

import static dagger.internal.Preconditions.checkNotNull;

/**
 * This class should be used to synchronize local persistent storage and remote storage.
 * For now, as we don't have backend present, it just decorates {@link ChatLocalDataSource}
 *
 * @author Gennady Denisov
 */
@Singleton
public class ChatRepository implements ChatDataSource {

    private final ChatDataSource mChatLocalDataSource;

    @Inject
    public ChatRepository(@Local ChatDataSource chatLocalDataSource) {
        mChatLocalDataSource = chatLocalDataSource;
    }

    @Override
    public void loadChat(@NonNull final ChatCallback chatCallback) {
        // Currently just assume that data loaded
        chatCallback.onDataLoaded();
    }

    @Override
    public void saveMessage(@NonNull final ChatMessage message) {
        checkNotNull(message);
        mChatLocalDataSource.saveMessage(message);
    }

    @Override
    public void deleteMessage(final long messageId) {
        mChatLocalDataSource.deleteMessage(messageId);
    }

    @Override
    public void deleteAll() {
        mChatLocalDataSource.deleteAll();
    }

    public interface LoadDataCallback {
        void onDataLoaded(Cursor data);

        void onDataEmpty();

        void onDataNotAvailable();

        void onDataReset();
    }
}
