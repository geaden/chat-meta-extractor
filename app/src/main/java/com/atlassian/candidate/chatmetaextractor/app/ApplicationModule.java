package com.atlassian.candidate.chatmetaextractor.app;

import android.content.Context;

import com.atlassian.candidate.chatmetaextractor.app.data.source.local.ChatLocalDataSource;

import dagger.Module;
import dagger.Provides;

/**
 * Dagger module to provide {@link Context} to pass to {@link ChatLocalDataSource}.
 *
 * @author Gennady Denisov
 */
@Module
public class ApplicationModule {

    private final Context mContext;

    ApplicationModule(final Context context) {
        mContext = context;
    }

    @Provides
    Context provideContext() {
        return mContext;
    }
}
