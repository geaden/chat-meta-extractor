package com.atlassian.candidate.chatmetaextractor.app;


public interface BasePresenter {

    void start();
}
