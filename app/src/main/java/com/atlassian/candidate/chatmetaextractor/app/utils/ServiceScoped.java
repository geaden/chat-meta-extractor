package com.atlassian.candidate.chatmetaextractor.app.utils;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Annotation for service scoped dagger injection.
 *
 * @author Gennady Denisov
 */
@Documented
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface ServiceScoped {
}
