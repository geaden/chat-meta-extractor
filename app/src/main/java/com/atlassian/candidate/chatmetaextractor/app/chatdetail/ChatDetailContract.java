package com.atlassian.candidate.chatmetaextractor.app.chatdetail;

import com.atlassian.candidate.chatmetaextractor.app.BasePresenter;
import com.atlassian.candidate.chatmetaextractor.app.BaseView;

import java.util.Date;

/**
 * Specifies contract between presenter and view.
 *
 * @author Gennady Denisov
 */
public interface ChatDetailContract {

    interface View extends BaseView<Presenter> {

        void setLoadingIndicator(boolean active);

        void showTitle(final Date createAt);

        void showBody(final String body);

        void hideCreatedAt();

        void hideBody();

        void openShareOptions(final String body);
    }

    interface Presenter extends BasePresenter {

        void shareChat();

    }
}
