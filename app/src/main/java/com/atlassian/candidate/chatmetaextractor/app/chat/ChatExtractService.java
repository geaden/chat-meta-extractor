package com.atlassian.candidate.chatmetaextractor.app.chat;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.atlassian.candidate.chatmetaextractor.app.ChatApplication;
import com.atlassian.candidate.extractor.MessageExtractor;
import com.atlassian.candidate.extractor.MetaExtractor;
import com.atlassian.candidate.extractor.impl.EmoticonsExtractor;
import com.atlassian.candidate.extractor.impl.LinksExtractor;
import com.atlassian.candidate.extractor.impl.MentionsExtractor;

import javax.inject.Inject;

/**
 * Service to extract meta information from a message by invoking {@link MetaExtractor#extract}.
 *
 * @author Gennady Denisov
 */
public class ChatExtractService extends IntentService {

    private static final String TAG = ChatExtractService.class.getSimpleName();

    public static final String MESSAGE_EXTRA = "message";
    public static final String RESULT_EXTRA = "result";
    public static final String ACTION_EXTRACT =
            "com.atlassian.candidate.chatmetaextractor.ACTION_EXTRACT";

    public static final String ACTION_RESULT =
            "com.atlassian.candidate.chatmetaextractor.ACTION_RESULT";

    private final MentionsExtractor mMentionsExtractor = new MentionsExtractor();
    private final EmoticonsExtractor mEmoticonsExtractor = new EmoticonsExtractor();

    @Inject
    LinksExtractor mLinksExtractor;

    public ChatExtractService() {
        super("ChatExtractService");
    }

    @Override
    public void onCreate() {
        super.onCreate();

        // Inject instance of {@link LinkExtractor}
        ((ChatApplication) getApplication())
                .getLinksExtractorComponent()
                .inject(this);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        final String message = intent.getStringExtra(MESSAGE_EXTRA);
        final MessageExtractor me =
                new MessageExtractor(mMentionsExtractor, mEmoticonsExtractor, mLinksExtractor);
        final String result = me.extract(message);
        final Intent resultIntent = new Intent(ACTION_RESULT);
        Log.d(TAG, "result: " + result);
        resultIntent.putExtra(RESULT_EXTRA, result);
        LocalBroadcastManager.getInstance(this).sendBroadcast(resultIntent);
    }

    public static void launchService(Context context, Intent intent) {
        context.startService(intent);
    }
}
