package com.atlassian.candidate.chatmetaextractor.app.about;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;

import com.atlassian.candidate.chatmetaextractor.app.BuildConfig;
import com.atlassian.candidate.chatmetaextractor.app.R;

/**
 * UI for about page.
 *
 * @author Gennady Denisov
 */
public class AboutFragment extends PreferenceFragment {

    private Preference mVersionPreference;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Load the preferences from an XML resource
        addPreferencesFromResource(R.xml.about);
        mVersionPreference = findPreference(getString(R.string.pref_key_version));
        findPreference(getString(R.string.pref_key_source))
                .setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                    @Override
                    public boolean onPreferenceClick(Preference preference) {
                        Intent intent = new Intent();
                        intent.setAction(Intent.ACTION_VIEW);
                        intent.setData(Uri.parse(getString(R.string.source_code_url)));
                        startActivity(intent);
                        return false;
                    }
                });
    }

    @Override
    public void onResume() {
        super.onResume();
        mVersionPreference.setSummary(BuildConfig.VERSION_NAME);
    }
}