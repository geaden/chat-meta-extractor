package com.atlassian.candidate.chatmetaextractor.app.data.source.local;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

import com.atlassian.candidate.chatmetaextractor.app.BuildConfig;

/**
 * The contract to save chat messages locally.
 *
 * @author Gennady Denisov
 */
public class ChatPersistenceContract {

    public static final String CONTENT_AUTHORITY = BuildConfig.APPLICATION_ID;

    public static final String CONTENT_SCHEME = "content://";

    public static final Uri BASE_CONTENT_URI = Uri.parse(CONTENT_SCHEME + CONTENT_AUTHORITY);

    // Definition of columns for chat entry
    public static abstract class ChatEntry implements BaseColumns {

        public static final String TABLE_NAME = "chat";

        public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + '/'
                + CONTENT_AUTHORITY + '/' + ChatEntry.TABLE_NAME;
        public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + '/'
                + CONTENT_AUTHORITY + '/' + ChatEntry.TABLE_NAME;


        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon()
                .appendPath(TABLE_NAME).build();

        public static final String COLUMN_NAME_MESSAGE = "message";
        public static final String COLUMN_NAME_BODY = "body";
        public static final String COLUMN_NAME_CREATED_AT = "created_at";


        public static String[] CHAT_COLUMNS = new String[]{
                ChatEntry._ID,
                COLUMN_NAME_MESSAGE,
                COLUMN_NAME_BODY,
                COLUMN_NAME_CREATED_AT,
        };

        public static Uri buildChatUriWithId(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }
}
