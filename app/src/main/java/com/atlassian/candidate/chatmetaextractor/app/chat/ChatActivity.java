package com.atlassian.candidate.chatmetaextractor.app.chat;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.atlassian.candidate.chatmetaextractor.app.ChatApplication;
import com.atlassian.candidate.chatmetaextractor.app.R;
import com.atlassian.candidate.chatmetaextractor.app.about.AboutActivity;
import com.atlassian.candidate.chatmetaextractor.app.data.source.LoaderProvider;

import javax.inject.Inject;

/**
 * Activity with list of chat messages.
 *
 * @author Gennady Denisov
 */
public class ChatActivity extends AppCompatActivity {

    @Inject
    ChatPresenter mChatPresenter;

    private DrawerLayout mDrawerLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_chat);

        // Set up the toolbar.
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar ab = getSupportActionBar();
        ab.setHomeAsUpIndicator(R.drawable.ic_menu);
        ab.setDisplayHomeAsUpEnabled(true);

        // Set up the navigation drawer.
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerLayout.setStatusBarBackground(R.color.colorPrimaryDark);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        if (navigationView != null) {
            setupDrawerContent(navigationView);
        }

        ChatFragment fragment = (ChatFragment) getSupportFragmentManager()
                .findFragmentById(R.id.fragment);
        fragment.setRetainInstance(true);

        // Create presenter and repository
        final LoaderProvider loaderProvider = new LoaderProvider(this);

        DaggerChatComponent.builder()
                .chatPresenterModule(
                        new ChatPresenterModule(loaderProvider, getSupportLoaderManager(), fragment))
                .chatRepositoryComponent(((ChatApplication) getApplication())
                        .getChatRepositoryComponent())
                .build().inject(this);

    }

    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        switch (menuItem.getItemId()) {
                            case R.id.chat_menu_item:
                                // Already on the screen
                                break;
                            case R.id.about_menu_item:
                                AboutActivity.launchActivity(ChatActivity.this);
                                break;
                            default:
                                break;
                        }
                        // Close the navigation drawer when an item is selected.
                        menuItem.setChecked(true);
                        mDrawerLayout.closeDrawers();
                        return true;
                    }
                });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // Open the navigation drawer when the home icon is selected from the toolbar.
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
