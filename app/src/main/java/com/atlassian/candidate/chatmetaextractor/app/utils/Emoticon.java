package com.atlassian.candidate.chatmetaextractor.app.utils;

/**
 * Represents emoticon.
 *
 * @author Gennady Denisov
 */
class Emoticon {
    String emoticon;
    int start;
    int end;

    Emoticon(String emoticon, int start, int end) {
        this.emoticon = emoticon;
        this.start = start;
        this.end = end;
    }
}
