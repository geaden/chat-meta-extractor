package com.atlassian.candidate.chatmetaextractor.app;

import android.app.Application;

import com.atlassian.candidate.chatmetaextractor.app.data.source.ChatRepositoryComponent;
import com.atlassian.candidate.chatmetaextractor.app.data.source.DaggerChatRepositoryComponent;

/**
 * Custom {@link Application} to store a singleton reference to the {@link LinksExtractorComponent}.
 *
 * @author Gennady Denisov
 */

public class ChatApplication extends Application {

    private LinksExtractorComponent mLinksExtractorComponent;

    private ChatRepositoryComponent mRepositoryComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        mLinksExtractorComponent = DaggerLinksExtractorComponent.create();

        mRepositoryComponent = DaggerChatRepositoryComponent.builder()
                .applicationModule(new ApplicationModule(getApplicationContext()))
                .build();
    }

    public LinksExtractorComponent getLinksExtractorComponent() {
        return mLinksExtractorComponent;
    }

    public ChatRepositoryComponent getChatRepositoryComponent() {
        return mRepositoryComponent;
    }
}
