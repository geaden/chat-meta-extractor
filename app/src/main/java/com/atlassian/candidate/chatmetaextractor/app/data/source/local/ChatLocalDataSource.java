package com.atlassian.candidate.chatmetaextractor.app.data.source.local;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.net.Uri;
import android.support.annotation.NonNull;

import com.atlassian.candidate.chatmetaextractor.app.data.ChatMessage;
import com.atlassian.candidate.chatmetaextractor.app.data.source.ChatDataSource;
import com.atlassian.candidate.chatmetaextractor.app.data.source.ChatValues;

import static dagger.internal.Preconditions.checkNotNull;

/**
 * Implementation of {@link ChatLocalDataSource} to store and retrieve messages from db.
 *
 * @author Gennady Denisov
 */
public class ChatLocalDataSource implements ChatDataSource {

    private static ChatLocalDataSource sInstance;

    private final ContentResolver mContentResolver;

    private ChatLocalDataSource(@NonNull final ContentResolver contentResolver) {
        checkNotNull(contentResolver);
        mContentResolver = contentResolver;
    }

    public static ChatLocalDataSource getInstance(@NonNull final ContentResolver contentResolver) {
        if (sInstance == null) {
            sInstance = new ChatLocalDataSource(contentResolver);
        }
        return sInstance;
    }

    @Override
    public void loadChat(@NonNull final ChatCallback chatCallback) {
        // Since data loaded using loader manage this method doesn't required implementation
    }

    @Override
    public void saveMessage(@NonNull final ChatMessage message) {
        checkNotNull(message);
        final ContentValues values = ChatValues.from(message);
        final Uri insertUri = mContentResolver.
                insert(ChatPersistenceContract.ChatEntry.CONTENT_URI, values);
        long chatId = ContentUris.parseId(insertUri);
        message.setId(chatId);
    }

    @Override
    public void deleteMessage(final long messageId) {

    }

    @Override
    public void deleteAll() {
        mContentResolver.delete(ChatPersistenceContract.ChatEntry.CONTENT_URI, null, null);
    }
}
