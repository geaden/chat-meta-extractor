package com.atlassian.candidate.chatmetaextractor.app.data.source;

import android.content.Context;

import com.atlassian.candidate.chatmetaextractor.app.data.source.local.ChatLocalDataSource;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * This is used by Dagger to inject arguments to a {@link ChatRepository}.
 *
 * @author Gennady Denisov
 */
@Module
public class ChatRepositoryModule {

    @Singleton
    @Provides
    @Local
    ChatDataSource provideChatLocalDataSource(Context context) {
        return ChatLocalDataSource.getInstance(context.getContentResolver());
    }
}
