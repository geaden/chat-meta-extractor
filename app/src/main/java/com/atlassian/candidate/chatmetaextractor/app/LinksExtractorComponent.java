package com.atlassian.candidate.chatmetaextractor.app;

import com.atlassian.candidate.chatmetaextractor.app.chat.ChatExtractService;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Dagger component used for injection of links extractor.
 *
 * @author Gennady Denisov
 */
@Singleton
@Component(modules = LinksExtractorModule.class)
public interface LinksExtractorComponent {

    void inject(ChatExtractService service);
}
