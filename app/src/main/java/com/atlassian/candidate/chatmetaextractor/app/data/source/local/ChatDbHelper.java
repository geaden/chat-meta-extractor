package com.atlassian.candidate.chatmetaextractor.app.data.source.local;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Helper to store chat in local database.
 *
 * @author Gennady Denisov
 */
public class ChatDbHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;

    public static final String DATABASE_NAME = "chat.db";

    private static final String INTEGER = " INTEGER ";
    private static final String PRIMARY_KEY = "PRIMARY KEY,";
    private static final String TEXT = " TEXT ";
    private static final String NOT_NULL = "NOT NULL,";


    private static final String SQL_CREATE_CHAT_TABLE =
            "CREATE TABLE " + ChatPersistenceContract.ChatEntry.TABLE_NAME + " ("
                    + ChatPersistenceContract.ChatEntry._ID + INTEGER + PRIMARY_KEY
                    + ChatPersistenceContract.ChatEntry.COLUMN_NAME_MESSAGE + TEXT + NOT_NULL
                    + ChatPersistenceContract.ChatEntry.COLUMN_NAME_BODY + TEXT + NOT_NULL
                    + ChatPersistenceContract.ChatEntry.COLUMN_NAME_CREATED_AT + " DATETIME DEFAULT CURRENT_TIMESTAMP);";


    public ChatDbHelper(final Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_CHAT_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // not required, but in real life should handle migrations if db schema changes
    }
}
