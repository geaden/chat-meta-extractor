package com.atlassian.candidate.chatmetaextractor.app.data.source;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;

import com.atlassian.candidate.chatmetaextractor.app.data.source.local.ChatPersistenceContract;

/**
 * Helper class to provide {@link CursorLoader}.
 *
 * @author Gennady Denisov
 */
public class LoaderProvider {

    @NonNull
    private final Context mContext;

    public LoaderProvider(@NonNull Context context) {
        mContext = context;
    }

    public Loader<Cursor> createChatLoader() {
        return new CursorLoader(
                mContext,
                ChatPersistenceContract.ChatEntry.CONTENT_URI,
                ChatPersistenceContract.ChatEntry.CHAT_COLUMNS, null, null,
                ChatPersistenceContract.ChatEntry.COLUMN_NAME_CREATED_AT + " DESC");
    }

    public Loader<Cursor> createChatDetailLoader(Uri chatUri) {
        return new CursorLoader(
                mContext,
                chatUri,
                ChatPersistenceContract.ChatEntry.CHAT_COLUMNS,
                null,
                null,
                null);
    }
}
