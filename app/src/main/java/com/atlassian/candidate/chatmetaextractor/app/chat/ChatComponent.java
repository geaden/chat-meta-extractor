package com.atlassian.candidate.chatmetaextractor.app.chat;

import com.atlassian.candidate.chatmetaextractor.app.data.source.ChatRepositoryComponent;
import com.atlassian.candidate.chatmetaextractor.app.utils.FragmentScoped;

import dagger.Component;

/**
 * Dagger component to inject into {@link ChatActivity}.
 *
 * @author Gennady Denisov
 */
@FragmentScoped
@Component(dependencies = ChatRepositoryComponent.class, modules = ChatPresenterModule.class)
public interface ChatComponent {

    void inject(ChatActivity activity);
}
