package com.atlassian.candidate.chatmetaextractor.app.chatdetail;

import com.atlassian.candidate.chatmetaextractor.app.utils.FragmentScoped;

import dagger.Component;

/**
 * Dagger component to inject {@link ChatDetailPresenter} into activity.
 *
 * @author Gennady Denisov
 */
@FragmentScoped
@Component(modules = ChatDetailPresenterModule.class)
public interface ChatDetailComponent {

    void inject(ChatDetailActivity activity);
}
