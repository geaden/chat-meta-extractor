package com.atlassian.candidate.chatmetaextractor.app;


public interface BaseView<T> {

    void setPresenter(T presenter);
}
