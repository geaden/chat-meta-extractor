package com.atlassian.candidate.chatmetaextractor.app.utils;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ImageSpan;
import android.widget.TextView;

import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;

/**
 * Custom target to load image into a text view.
 */
class EmoticonSpanTarget extends SimpleTarget<Bitmap> {

    private final Emoticon emoticon;
    private final TextView tv;
    private final SpannableStringBuilder ssb;

    EmoticonSpanTarget(final Emoticon emoticon, final TextView tv, final SpannableStringBuilder ssb) {
        this.emoticon = emoticon;
        this.tv = tv;
        this.ssb = ssb;
    }

    @Override
    public void onResourceReady(final Bitmap bitmap,
                                final GlideAnimation<? super Bitmap> glideAnimation) {
        BitmapDrawable bitmapDrawable = new BitmapDrawable(tv.getContext().getResources(), bitmap);
        // image span doesn't handle scaling so we manually set bounds
        int height = tv.getLineHeight();
        int width = tv.getLineHeight() * bitmap.getWidth() / bitmap.getHeight();
        bitmapDrawable.setBounds(0, 0, width, height);
        ssb.setSpan(new ImageSpan(bitmapDrawable, ImageSpan.ALIGN_BASELINE),
                emoticon.start, emoticon.end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        tv.setText(ssb, TextView.BufferType.SPANNABLE);
    }
}
