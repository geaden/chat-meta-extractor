package com.atlassian.candidate.chatmetaextractor.app.chat;

import android.database.Cursor;
import android.support.annotation.NonNull;

import com.atlassian.candidate.chatmetaextractor.app.BasePresenter;
import com.atlassian.candidate.chatmetaextractor.app.BaseView;
import com.atlassian.candidate.chatmetaextractor.app.data.ChatMessage;

/**
 * Contract between view and presenter.
 *
 * @author Gennady Denisov
 */
public interface ChatContract {

    interface View extends BaseView<Presenter> {

        void setLoadingIndicator(boolean active);

        void showMessages(Cursor cursor);

        void showLoadingError();

        void showMessageDetailsUi(@NonNull ChatMessage message);

        void showNoMessages();

        void startExtractService(@NonNull ChatMessage message, @NonNull ChatExtractCallback callback);

        void resetSendMessage();

        void showExtracting(boolean active);
    }

    interface Presenter extends BasePresenter {

        void loadMessages();

        void sendMessage(@NonNull ChatMessage message);

        void openMessageDetails(@NonNull ChatMessage message);
    }
}
