package com.atlassian.candidate.chatmetaextractor.app.chat;

import android.support.v4.app.LoaderManager;

import com.atlassian.candidate.chatmetaextractor.app.data.source.LoaderProvider;

import dagger.Module;
import dagger.Provides;

/**
 * Dagger module to pass View, LoaderProvider and LoaderManager into {@link ChatPresenter}.
 *
 * @author Gennady Denisov
 */
@Module
public class ChatPresenterModule {

    private final ChatContract.View mView;

    private final LoaderProvider mLoaderProvider;

    private final LoaderManager mLoaderManager;

    public ChatPresenterModule(LoaderProvider loaderProvider, LoaderManager loaderManager,
                               ChatContract.View view) {
        mLoaderProvider = loaderProvider;
        mLoaderManager = loaderManager;
        mView = view;
    }

    @Provides
    ChatContract.View provideChatContractView() {
        return mView;
    }

    @Provides
    LoaderManager provideLoaderManager() {
        return mLoaderManager;
    }

    @Provides
    LoaderProvider provideLoaderProvider() {
        return mLoaderProvider;
    }
}
