package com.atlassian.candidate.chatmetaextractor.app.utils;

import android.content.Context;
import android.text.SpannableStringBuilder;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Emoticons related utils.
 *
 * @author Gennady Denisov
 */
public final class EmoticonsUtils {

    private EmoticonsUtils() {
        // Prevent instantiation
    }

    /**
     * Method to set a {@link SpannableStringBuilder} from a string to a {@link TextView}.
     *
     * @param tv   text view to set spannable to.
     * @param text text to be replaced.
     */
    public static void setSpannable(final TextView tv, final String text) {
        Map<String, String> emoticonsMap;
        SpannableStringBuilder builder = new SpannableStringBuilder(text);
        try {
            emoticonsMap = getEmoticonsMap(tv.getContext());
        } catch (IOException e) {
            // Failed to get emoticons map
            tv.setText(builder, TextView.BufferType.SPANNABLE);
            return;
        }
        // Locate all positions of emoticons
        final List<Emoticon> emoticons = new ArrayList<>();
        final Pattern pattern = Pattern.compile("\\(\\w+\\)");
        final Matcher match = pattern.matcher(text);
        while (match.find()) {
            emoticons.add(new Emoticon(match.group(), match.start(), match.end()));
        }
        if (!emoticons.isEmpty()) {
            // Append emoticons
            for (Emoticon emoticon : emoticons) {
                final EmoticonSpanTarget target = new EmoticonSpanTarget(emoticon, tv, builder);
                if (emoticonsMap.containsKey(emoticon.emoticon.toLowerCase(Locale.getDefault()))) {
                    final String emoticonUrl = emoticonsMap.get(emoticon.emoticon);
                    Glide.with(tv.getContext())
                            .load(emoticonUrl)
                            .asBitmap()
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .centerCrop()
                            .dontAnimate()
                            .into(target);
                } else {
                    Glide.clear(target);
                }
            }
        }
        tv.setText(builder, TextView.BufferType.SPANNABLE);
    }

    @SuppressWarnings("unchecked")
    private static Map<String, String> getEmoticonsMap(final Context ctx) throws IOException {
        final InputStream is = ctx.getAssets().open("emoticons.json");
        final Reader reader = new InputStreamReader(is, "UTF-8");
        return new Gson().fromJson(reader, Map.class);
    }

}
