package com.atlassian.candidate.chatmetaextractor.app.chat;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.net.Uri;
import android.os.Bundle;
import android.provider.BaseColumns;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.atlassian.candidate.chatmetaextractor.app.R;
import com.atlassian.candidate.chatmetaextractor.app.chatdetail.ChatDetailActivity;
import com.atlassian.candidate.chatmetaextractor.app.data.ChatMessage;
import com.atlassian.candidate.chatmetaextractor.app.data.source.local.ChatPersistenceContract;
import com.atlassian.candidate.chatmetaextractor.app.utils.EmoticonsUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Display list of chat messages.
 *
 * @author Gennady Denisov
 */
public class ChatFragment extends Fragment implements ChatContract.View {

    private static final String TAG = ChatFragment.class.getSimpleName();

    private ChatContract.Presenter mPresenter;

    private ChatAdapter mChatAdapter;

    @BindView(R.id.chat)
    RecyclerView mChatRecyclerView;

    @BindView(R.id.message)
    EditText mMessageTextView;

    @BindView(R.id.send)
    ImageButton mSendButton;

    @BindView(R.id.empty_messages)
    TextView mEmptyMessagesTextView;

    @BindView(R.id.refresh_layout)
    SwipeRefreshLayout mSwipeRefreshLayout;

    private ChatExtractCallback mExtractCallback;

    public ChatFragment() {
        setHasOptionsMenu(true);
    }

    public static ChatFragment newInstance() {
        return new ChatFragment();
    }

    private BroadcastReceiver mExtractReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(final Context context, final Intent intent) {
            if (ChatExtractService.ACTION_RESULT.equals(intent.getAction())) {
                final String message = intent.getStringExtra(ChatExtractService.RESULT_EXTRA);
                Log.d(TAG, "Result received " + message);
                // Update chat message and store in database
                if (mExtractCallback != null) {
                    mExtractCallback.onExtracted(message);
                }
            }
        }
    };

    // Setup listener for chat items clicks
    private ChatItemListener mItemListener = new ChatItemListener() {
        @Override
        public void onChatMessageClick(final ChatMessage clickedMessage) {
            mPresenter.openMessageDetails(clickedMessage);
        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mChatAdapter = new ChatAdapter(getActivity(), null, mItemListener);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.frag_chat, container, false);
        ButterKnife.bind(this, rootView);

        // Setup recycler view
        mChatRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(),
                DividerItemDecoration.VERTICAL));
        mChatRecyclerView.setHasFixedSize(true);
        mChatRecyclerView.setAdapter(mChatAdapter);
        mChatRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        // Setup listener for message text
        mMessageTextView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(final CharSequence s, final int start, final int count,
                                          final int after) {

            }

            @Override
            public void onTextChanged(final CharSequence s, final int start, final int before,
                                      final int count) {
                boolean canSend = true;
                if (s == null || TextUtils.isEmpty(s)) {
                    canSend = false;
                }
                Log.d(TAG, "Send is available " + canSend);
                mSendButton.setClickable(canSend);
            }

            @Override
            public void afterTextChanged(final Editable s) {
                // Do nothing...
            }
        });

        mSendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String message = mMessageTextView.getText().toString();
                final ChatMessage chatMessage = new ChatMessage();
                chatMessage.setMessage(message);
                mPresenter.sendMessage(chatMessage);
            }
        });

        // Set up progress indicator
        mSwipeRefreshLayout.setColorSchemeColors(
                ContextCompat.getColor(getActivity(), R.color.colorPrimary),
                ContextCompat.getColor(getActivity(), R.color.colorAccent),
                ContextCompat.getColor(getActivity(), R.color.colorPrimaryDark));

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mPresenter.loadMessages();
            }
        });

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        // Register receiver
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mExtractReceiver,
                new IntentFilter(ChatExtractService.ACTION_RESULT));
        mPresenter.start();
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mExtractReceiver);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }


    @Override
    public void showMessages(final Cursor cursor) {
        mEmptyMessagesTextView.setVisibility(View.GONE);
        mChatAdapter.swapCursor(cursor);
    }

    @Override
    public void showLoadingError() {

    }

    @Override
    public void resetSendMessage() {
        mMessageTextView.setText(null);
        mSendButton.setClickable(false);
    }

    @Override
    public void showMessageDetailsUi(final ChatMessage message) {
        final Uri chatUri = ChatPersistenceContract.ChatEntry.buildChatUriWithId(message.getId());
        ChatDetailActivity.launchActivity(getActivity(), chatUri);
    }

    @Override
    public void startExtractService(final ChatMessage message, final ChatExtractCallback callback) {
        mExtractCallback = callback;
        Intent intent = new Intent(getActivity(), ChatExtractService.class);
        intent.setAction(ChatExtractService.ACTION_EXTRACT);
        intent.putExtra(ChatExtractService.MESSAGE_EXTRA, message.getMessage());
        ChatExtractService.launchService(getActivity(), intent);
    }

    @Override
    public void showExtracting(final boolean active) {
        if (getView() == null) {
            return;
        }
        if (active) {
            Snackbar sb = Snackbar.make(getView(), getText(R.string.extracting),
                    Snackbar.LENGTH_INDEFINITE);
            Snackbar.SnackbarLayout sbl = (Snackbar.SnackbarLayout) sb.getView();
            sbl.addView(new ProgressBar(getActivity()));
            sb.show();
        } else {
            Snackbar.make(getView(), getText(R.string.extracted), Snackbar.LENGTH_SHORT).show();
        }
    }

    @Override
    public void showNoMessages() {
        mEmptyMessagesTextView.setVisibility(View.VISIBLE);
    }

    @Override
    public void setLoadingIndicator(final boolean active) {
        // Make sure setRefreshing() is called after the layout is done with everything else.
        mSwipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                mSwipeRefreshLayout.setRefreshing(active);
            }
        });

    }

    @Override
    public void setPresenter(ChatContract.Presenter presenter) {
        mPresenter = presenter;
    }


    static class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ChatViewHolder> {

        private Cursor mCursor;

        private Context mContext;

        private boolean mDataValid;

        private int mRowIdColumn;

        private DataSetObserver mDataSetObserver;

        private ChatItemListener mItemListener;

        ChatAdapter(Context context, Cursor cursor, ChatItemListener itemListener) {
            mContext = context;
            mCursor = cursor;
            mDataValid = null != cursor;
            mRowIdColumn = mDataValid ? mCursor.getColumnIndex(BaseColumns._ID) : -1;
            mItemListener = itemListener;
            mDataSetObserver = new NotifyingDataSetObserver();
            if (mCursor != null) {
                mCursor.registerDataSetObserver(mDataSetObserver);
            }
        }

        @Override
        public ChatViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            final View itemView = LayoutInflater.from(mContext)
                    .inflate(R.layout.chat_item, parent, false);
            return new ChatViewHolder(itemView, mItemListener);
        }

        @Override
        public void onBindViewHolder(ChatViewHolder holder, int position) {
            if (!mDataValid) {
                throw new IllegalArgumentException();
            }
            if (!mCursor.moveToPosition(position)) {
                throw new IllegalArgumentException();
            }
            final ChatMessage message = ChatMessage.from(mCursor);
            EmoticonsUtils.setSpannable(holder.mMessage, message.getMessage());
            holder.mCreated.setText(DateUtils.getRelativeTimeSpanString(
                    message.getCreated().getTime()));
        }

        void swapCursor(Cursor newCursor) {
            if (newCursor == mCursor) {
                return;
            }
            final Cursor oldCursor = mCursor;
            if (oldCursor != null && mDataSetObserver != null) {
                oldCursor.unregisterDataSetObserver(mDataSetObserver);
            }
            mCursor = newCursor;
            if (mCursor != null) {
                if (mDataSetObserver != null) {
                    mCursor.registerDataSetObserver(mDataSetObserver);
                }
                mRowIdColumn = newCursor.getColumnIndex(BaseColumns._ID);
                mDataValid = true;
            } else {
                mRowIdColumn = -1;
                mDataValid = false;
            }
            notifyDataSetChanged();
            if (oldCursor != null) {
                oldCursor.close();
            }
        }

        @Override
        public long getItemId(final int position) {
            if (mDataValid && mCursor != null && mCursor.moveToPosition(position)) {
                return mCursor.getLong(mRowIdColumn);
            }
            return super.getItemId(position);
        }

        @Override
        public int getItemCount() {
            if (mDataValid && mCursor != null) {
                return mCursor.getCount();
            }
            return 0;
        }

        public ChatMessage getItem(final int position) {
            if (mDataValid && mCursor != null && mCursor.moveToPosition(position)) {
                mCursor.moveToPosition(position);
                return ChatMessage.from(mCursor);
            }
            return null;
        }

        private class NotifyingDataSetObserver extends DataSetObserver {
            @Override
            public void onChanged() {
                super.onChanged();
                mDataValid = true;
                notifyDataSetChanged();
            }

            @Override
            public void onInvalidated() {
                super.onInvalidated();
                mDataValid = false;
                notifyDataSetChanged();
            }
        }

        class ChatViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

            @BindView(R.id.userpic)
            ImageView mUserpic;

            @BindView(R.id.message)
            TextView mMessage;

            @BindView(R.id.created)
            TextView mCreated;

            private ChatItemListener mItemListener;

            ChatViewHolder(View itemView, ChatItemListener itemListener) {
                super(itemView);
                ButterKnife.bind(this, itemView);
                mItemListener = itemListener;
                itemView.setOnClickListener(this);
            }

            @Override
            public void onClick(final View v) {
                int position = getAdapterPosition();
                final ChatMessage message = getItem(position);
                mItemListener.onChatMessageClick(message);
            }
        }
    }

    /**
     * A listener for chat items clicks.
     */
    public interface ChatItemListener {

        void onChatMessageClick(ChatMessage clickedMessage);
    }
}
