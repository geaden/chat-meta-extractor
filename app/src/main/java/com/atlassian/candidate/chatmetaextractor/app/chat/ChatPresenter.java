package com.atlassian.candidate.chatmetaextractor.app.chat;

import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;

import com.atlassian.candidate.chatmetaextractor.app.data.ChatMessage;
import com.atlassian.candidate.chatmetaextractor.app.data.source.ChatDataSource;
import com.atlassian.candidate.chatmetaextractor.app.data.source.ChatRepository;
import com.atlassian.candidate.chatmetaextractor.app.data.source.LoaderProvider;

import javax.inject.Inject;

import static dagger.internal.Preconditions.checkNotNull;

/**
 * Presenter to interact with UI.
 *
 * @author Gennady Denisov
 */
public class ChatPresenter implements ChatContract.Presenter, LoaderManager.LoaderCallbacks<Cursor>,
        ChatRepository.LoadDataCallback, ChatDataSource.ChatCallback {

    private static final int CHAT_LOADER = 1;

    private final ChatContract.View mChatView;

    @NonNull
    private final ChatRepository mChatRepository;

    @NonNull
    private final LoaderManager mLoaderManager;

    @NonNull
    private final LoaderProvider mLoaderProvider;


    @Inject
    public ChatPresenter(@NonNull LoaderProvider loaderProvider, @NonNull LoaderManager loaderManager,
                         @NonNull ChatContract.View chatMessagesView,
                         @NonNull ChatRepository chatRepository) {
        mLoaderProvider = loaderProvider;
        mLoaderManager = loaderManager;
        mChatRepository = chatRepository;
        mChatView = chatMessagesView;
    }

    @Inject
    void setupListeners() {
        mChatView.setPresenter(this);
    }

    @Override
    public void loadMessages() {
        mChatView.setLoadingIndicator(true);
        mChatRepository.loadChat(this);
    }

    @Override
    public void sendMessage(final ChatMessage message) {
        checkNotNull(message, "message cannot be null");
        mChatView.showExtracting(true);
        mChatView.startExtractService(message, new ChatExtractCallback() {

            @Override
            public void onExtracted(final String body) {
                // Update message and save to repository
                mChatView.showExtracting(false);
                message.setBody(body);
                mChatRepository.saveMessage(message);
                mChatView.resetSendMessage();
            }
        });
    }

    @Override
    public void openMessageDetails(@NonNull final ChatMessage message) {
        mChatView.showMessageDetailsUi(message);
    }

    @Override
    public void start() {
        loadMessages();
    }

    @Override
    public void onDataLoaded() {
        if (mLoaderManager.getLoader(CHAT_LOADER) == null) {
            mLoaderManager.initLoader(CHAT_LOADER, null, this);
        } else {
            mLoaderManager.restartLoader(CHAT_LOADER, null, this);
        }
    }

    @Override
    public void onDataLoaded(final Cursor data) {
        mChatView.setLoadingIndicator(false);
        mChatView.showMessages(data);
    }

    @Override
    public void onDataEmpty() {
        mChatView.setLoadingIndicator(false);
        mChatView.showNoMessages();
    }

    @Override
    public void onDataNotAvailable() {
        mChatView.setLoadingIndicator(false);
        mChatView.showLoadingError();

    }

    @Override
    public void onDataReset() {
        mChatView.showMessages(null);
    }

    @Override
    public Loader<Cursor> onCreateLoader(final int id, final Bundle args) {
        return mLoaderProvider.createChatLoader();
    }

    @Override
    public void onLoadFinished(final Loader<Cursor> loader, final Cursor data) {
        if (data != null) {
            if (data.moveToLast()) {
                onDataLoaded(data);
            } else {
                onDataEmpty();
            }
        } else {
            onDataNotAvailable();
        }

    }

    @Override
    public void onLoaderReset(final Loader<Cursor> loader) {
        onDataReset();
    }
}
