package com.atlassian.candidate.chatmetaextractor.app.chatdetail;

import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;

import com.atlassian.candidate.chatmetaextractor.app.data.ChatMessage;
import com.atlassian.candidate.chatmetaextractor.app.data.source.ChatRepository;
import com.atlassian.candidate.chatmetaextractor.app.data.source.LoaderProvider;

import java.util.Date;

import javax.inject.Inject;

/**
 * Present to interact with UI.
 *
 * @author Gennady Denisov
 */
class ChatDetailPresenter implements ChatDetailContract.Presenter, LoaderManager.LoaderCallbacks<Cursor>,
        ChatRepository.LoadDataCallback {

    private static final int CHATDETAIL_LOADER = 2;

    private ChatDetailContract.View mView;

    @NonNull
    private LoaderManager mLoaderManager;

    @NonNull
    private Uri mChatUri;

    private ChatMessage mMessage;

    @NonNull
    private LoaderProvider mLoaderProvider;

    @Inject
    public ChatDetailPresenter(Uri chatUri, @NonNull final LoaderProvider loaderProvider,
                               @NonNull final LoaderManager loaderManager,
                               final ChatDetailContract.View view) {
        mChatUri = chatUri;
        mView = view;
        mLoaderManager = loaderManager;
        mLoaderProvider = loaderProvider;
        mView.setPresenter(this);
    }

    @Override
    public void start() {
        loadChat();
    }

    private void loadChat() {
        mView.setLoadingIndicator(true);
        mLoaderManager.initLoader(CHATDETAIL_LOADER, null, this);
    }

    @Override
    public void shareChat() {
        if (mMessage != null) {
            mView.openShareOptions(mMessage.getBody());
        }
    }

    private void showChat(final Cursor cursor) {
        mMessage = ChatMessage.from(cursor);
        final String body = mMessage.getBody();
        final Date createdAt = mMessage.getCreated();

        mView.showBody(body);
        mView.showTitle(createdAt);

        mView.setLoadingIndicator(false);
    }

    @Override
    public void onDataLoaded(final Cursor data) {
        showChat(data);
    }

    @Override
    public void onDataEmpty() {
        mView.setLoadingIndicator(false);
        mView.hideBody();
        mView.hideCreatedAt();
    }

    @Override
    public void onDataNotAvailable() {
        mView.setLoadingIndicator(false);

    }

    @Override
    public void onDataReset() {
        // Do nothing...
    }

    @Override
    public Loader<Cursor> onCreateLoader(final int id, final Bundle args) {
        return mLoaderProvider.createChatDetailLoader(mChatUri);
    }

    @Override
    public void onLoadFinished(final Loader<Cursor> loader, final Cursor data) {
        if (data != null) {
            if (data.moveToLast()) {
                onDataLoaded(data);
            } else {
                onDataEmpty();
            }
        } else {
            onDataNotAvailable();
        }
    }

    @Override
    public void onLoaderReset(final Loader<Cursor> loader) {
        onDataReset();
    }
}
