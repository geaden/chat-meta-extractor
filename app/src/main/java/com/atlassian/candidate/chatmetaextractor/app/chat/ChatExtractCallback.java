package com.atlassian.candidate.chatmetaextractor.app.chat;

/**
 * Helper interface to update chat after meta extracted.
 *
 * @author Gennady Denisov
 */
public interface ChatExtractCallback {

    void onExtracted(final String body);
}
