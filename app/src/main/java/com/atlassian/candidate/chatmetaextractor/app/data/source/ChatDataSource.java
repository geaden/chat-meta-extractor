package com.atlassian.candidate.chatmetaextractor.app.data.source;

import android.support.annotation.NonNull;

import com.atlassian.candidate.chatmetaextractor.app.data.ChatMessage;

/**
 * Entry point to store and delete a chat message.
 *
 * @author Gennady Denisov
 */
public interface ChatDataSource {

    interface ChatCallback {

        void onDataLoaded();
    }

    void loadChat(@NonNull ChatCallback chatCallback);

    void saveMessage(@NonNull final ChatMessage message);

    void deleteMessage(long messageId);

    void deleteAll();
}
