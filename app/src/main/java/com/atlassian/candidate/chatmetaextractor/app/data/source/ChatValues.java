package com.atlassian.candidate.chatmetaextractor.app.data.source;

import android.content.ContentValues;

import com.atlassian.candidate.chatmetaextractor.app.data.ChatMessage;
import com.atlassian.candidate.chatmetaextractor.app.data.source.local.ChatPersistenceContract;

/**
 * Helper class to obtain {@link ContentValues} from a {@link ChatMessage}.
 *
 * @author Gennady Denisov
 */
public class ChatValues {

    public static ContentValues from(final ChatMessage message) {
        ContentValues values = new ContentValues();
        values.put(ChatPersistenceContract.ChatEntry.COLUMN_NAME_MESSAGE, message.getMessage());
        values.put(ChatPersistenceContract.ChatEntry.COLUMN_NAME_BODY, message.getBody());
        return values;
    }

}
