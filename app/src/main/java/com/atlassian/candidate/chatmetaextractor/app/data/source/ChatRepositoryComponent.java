package com.atlassian.candidate.chatmetaextractor.app.data.source;

import com.atlassian.candidate.chatmetaextractor.app.ApplicationModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Dagger component.
 *
 * @author Gennady Denisov
 */
@Singleton
@Component(modules = {ChatRepositoryModule.class, ApplicationModule.class})
public interface ChatRepositoryComponent {

    ChatRepository getChatRepository();
}
