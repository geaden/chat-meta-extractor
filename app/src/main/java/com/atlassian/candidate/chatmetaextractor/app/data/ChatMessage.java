package com.atlassian.candidate.chatmetaextractor.app.data;

import android.database.Cursor;
import android.support.annotation.NonNull;
import android.util.Log;

import com.atlassian.candidate.chatmetaextractor.app.data.source.local.ChatPersistenceContract;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * POJO representing a single chat message.
 *
 * @author Gennady Denisov
 */
public class ChatMessage {

    private static final String TAG = ChatMessage.class.getSimpleName();

    @NonNull
    private Long id;

    @NonNull
    private String message;

    // A JSON body extracted from the message
    @NonNull
    private String body;

    @NonNull
    private Date created;

    @NonNull
    public Long getId() {
        return id;
    }

    public void setId(@NonNull Long id) {
        this.id = id;
    }

    @NonNull
    public String getBody() {
        return body;
    }

    public void setBody(@NonNull final String body) {
        this.body = body;
    }

    @NonNull
    public String getMessage() {
        return message;
    }

    public void setMessage(@NonNull final String message) {
        this.message = message;
    }

    @NonNull
    public Date getCreated() {
        return created;
    }

    public void setCreated(@NonNull final Date created) {
        this.created = created;
    }

    public static synchronized ChatMessage from(final Cursor cursor) {
        final ChatMessage chatMessage = new ChatMessage();
        chatMessage.setId(cursor.getLong(
                cursor.getColumnIndex(ChatPersistenceContract.ChatEntry._ID)));
        chatMessage.setBody(cursor.getString(cursor.getColumnIndex(
                ChatPersistenceContract.ChatEntry.COLUMN_NAME_BODY)));
        chatMessage.setMessage(cursor.getString(cursor.getColumnIndex(
                ChatPersistenceContract.ChatEntry.COLUMN_NAME_MESSAGE)));
        final DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        String cursorDatetime = cursor.getString(
                cursor.getColumnIndex(ChatPersistenceContract.ChatEntry.COLUMN_NAME_CREATED_AT));
        try {
            long when = df.parse(cursorDatetime).getTime();
            chatMessage.setCreated(new Date(when + TimeZone.getDefault().getOffset(when)));
        } catch (ParseException e) {
            Log.w(TAG, "Unable to parse datetime string " + cursorDatetime, e);
        }
        return chatMessage;
    }

    @Override
    public String toString() {
        return "ChatMessage{" +
                "id=" + id +
                ", message='" + message + '\'' +
                ", body='" + body + '\'' +
                ", created=" + created +
                '}';
    }
}
