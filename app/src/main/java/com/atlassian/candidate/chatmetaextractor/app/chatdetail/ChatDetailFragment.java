package com.atlassian.candidate.chatmetaextractor.app.chatdetail;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.ShareCompat;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.atlassian.candidate.chatmetaextractor.app.R;

import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * View to show chat detail.
 *
 * @author Gennady Denisov
 */
public class ChatDetailFragment extends Fragment implements ChatDetailContract.View {

    private ChatDetailContract.Presenter mPresenter;

    @BindView(R.id.chat_body)
    TextView mChatBody;

    @BindView(R.id.loading)
    ProgressBar mLoading;

    public ChatDetailFragment() {
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable final ViewGroup container,
                             @Nullable final Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.frag_chatdetail, container, false);
        ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.start();
    }

    @Override
    public void onCreateOptionsMenu(final Menu menu, final MenuInflater inflater) {
        inflater.inflate(R.menu.menu_chat, menu);
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.chat_share) {
            mPresenter.shareChat();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void setPresenter(final ChatDetailContract.Presenter presenter) {
        mPresenter = presenter;

    }

    @Override
    public void setLoadingIndicator(final boolean active) {
        mLoading.setVisibility(active ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showTitle(final Date createAt) {
        final String title = DateUtils.getRelativeTimeSpanString(createAt.getTime()).toString();
        ((ToolbarCallback) getActivity()).updateTitle(title);
    }

    @Override
    public void showBody(final String body) {
        mChatBody.setText(body);
    }

    @Override
    public void hideCreatedAt() {
        ((ToolbarCallback) getActivity()).updateTitle(null);
    }

    @Override
    public void hideBody() {
        mChatBody.setText(null);
    }

    @Override
    public void openShareOptions(final String body) {
        ShareCompat.IntentBuilder.from(getActivity())
                .setChooserTitle(R.string.chat_share_title)
                .setSubject(getString(R.string.chat_share_title))
                .setType("text/plain")
                .setText(body)
                .startChooser();
    }

    /**
     * A callback to update toolbar title
     */
    public interface ToolbarCallback {

        void updateTitle(final String title);
    }
}
