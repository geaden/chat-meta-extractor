package com.atlassian.candidate.chatmetaextractor.app.data.source;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.Nullable;

import com.atlassian.candidate.chatmetaextractor.app.data.source.local.ChatDbHelper;
import com.atlassian.candidate.chatmetaextractor.app.data.source.local.ChatPersistenceContract;

/**
 * Content provider for chat messages.
 *
 * @author Gennady Denisov
 */
public class ChatProvider extends ContentProvider {

    // Chat
    private static final int CHAT_MESSAGE = 100;
    private static final int CHAT_MESSAGE_ITEM = 101;

    private static final UriMatcher sUriMatcher = buildUriMatcher();

    private static final String sChatSelection
            = ChatPersistenceContract.ChatEntry._ID + " = ?";

    private ChatDbHelper mChatDbHelper;

    private static UriMatcher buildUriMatcher() {
        final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
        final String authority = ChatPersistenceContract.CONTENT_AUTHORITY;

        matcher.addURI(authority, ChatPersistenceContract.ChatEntry.TABLE_NAME, CHAT_MESSAGE);
        matcher.addURI(authority, ChatPersistenceContract.ChatEntry.TABLE_NAME + "/#", CHAT_MESSAGE_ITEM);

        return matcher;
    }


    @Override
    public boolean onCreate() {
        mChatDbHelper = new ChatDbHelper(getContext());
        return true;
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        switch (match(uri)) {
            case CHAT_MESSAGE:
                return ChatPersistenceContract.ChatEntry.CONTENT_TYPE;
            case CHAT_MESSAGE_ITEM:
                return ChatPersistenceContract.ChatEntry.CONTENT_ITEM_TYPE;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        Cursor retCursor;
        String[] where;
        switch (match(uri)) {
            case CHAT_MESSAGE:
                retCursor = mChatDbHelper.getReadableDatabase().query(
                        ChatPersistenceContract.ChatEntry.TABLE_NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder
                );
                break;
            case CHAT_MESSAGE_ITEM:
                where = new String[]{Long.toString(ContentUris.parseId(uri))};
                retCursor = mChatDbHelper.getReadableDatabase().query(
                        ChatPersistenceContract.ChatEntry.TABLE_NAME,
                        projection,
                        sChatSelection,
                        where,
                        null,
                        null,
                        sortOrder
                );
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        retCursor.setNotificationUri(getContext().getContentResolver(), uri);
        return retCursor;
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        final SQLiteDatabase db = mChatDbHelper.getWritableDatabase();
        Uri returnUri;

        if (match(uri) == CHAT_MESSAGE) {
            long _chatId = db.insert(ChatPersistenceContract.ChatEntry.TABLE_NAME, null, values);
            if (_chatId > 0) {
                returnUri = ChatPersistenceContract.ChatEntry.buildChatUriWithId(_chatId);
            } else {
                throw new android.database.SQLException("Failed to insert row into " + uri);
            }
        } else {
            throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return returnUri;
    }

    private int match(final Uri uri) {
        return sUriMatcher.match(uri);
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        if (match(uri) != CHAT_MESSAGE) {
            throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        return mChatDbHelper.getWritableDatabase().delete(
                ChatPersistenceContract.ChatEntry.TABLE_NAME,
                null,
                null);
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        return 0;
    }
}
