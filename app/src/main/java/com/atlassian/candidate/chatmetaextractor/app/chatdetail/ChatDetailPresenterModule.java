package com.atlassian.candidate.chatmetaextractor.app.chatdetail;


import android.net.Uri;
import android.support.v4.app.LoaderManager;

import com.atlassian.candidate.chatmetaextractor.app.data.source.LoaderProvider;

import dagger.Module;
import dagger.Provides;

/**
 * Dagger module to pass View, LoaderManager and LoaderProvider into {@link ChatDetailPresenter}.
 *
 * @author Gennady Denisov
 */
@Module
public class ChatDetailPresenterModule {

    private Uri mChatUri;

    private LoaderProvider mLoaderProvider;

    private LoaderManager mLoaderManager;

    private ChatDetailContract.View mView;

    public ChatDetailPresenterModule(final Uri chatUri, final LoaderProvider loaderProvider,
                                     final LoaderManager loaderManager, final ChatDetailContract.View view) {
        mChatUri = chatUri;
        mLoaderProvider = loaderProvider;
        mLoaderManager = loaderManager;
        mView = view;
    }

    @Provides
    Uri provideChatUri() {
        return mChatUri;
    }

    @Provides
    ChatDetailContract.View provideChatDetailContractView() {
        return mView;
    }

    @Provides
    LoaderManager provideLoaderManager() {
        return mLoaderManager;
    }

    @Provides
    LoaderProvider provideLoaderProvider() {
        return mLoaderProvider;
    }
}
