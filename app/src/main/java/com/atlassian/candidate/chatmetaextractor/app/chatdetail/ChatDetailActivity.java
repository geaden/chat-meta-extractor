package com.atlassian.candidate.chatmetaextractor.app.chatdetail;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.atlassian.candidate.chatmetaextractor.app.R;
import com.atlassian.candidate.chatmetaextractor.app.data.source.LoaderProvider;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Main entry point to display chat detail.
 *
 * @author Gennady Denisov
 */
public class ChatDetailActivity extends AppCompatActivity implements ChatDetailFragment.ToolbarCallback {

    @Inject
    ChatDetailPresenter mPresenter;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    public static void launchActivity(final Context context, final Uri mChatUri) {
        Intent intent = new Intent(context, ChatDetailActivity.class);
        intent.setData(mChatUri);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_chatdetail);
        ButterKnife.bind(this);

        // Set up the toolbar.
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setDisplayShowHomeEnabled(true);

        // Get chat Uri
        Uri mChatUri = getIntent().getData();

        ChatDetailFragment fragment = (ChatDetailFragment) getSupportFragmentManager()
                .findFragmentById(R.id.fragment);

        final LoaderProvider loaderProvider = new LoaderProvider(this);

        DaggerChatDetailComponent.builder()
                .chatDetailPresenterModule(
                        new ChatDetailPresenterModule(mChatUri, loaderProvider,
                                getSupportLoaderManager(), fragment))
                .build().inject(this);
    }

    @Override
    public void updateTitle(final String title) {
        final ActionBar ab = getSupportActionBar();
        if (null != ab) {
            ab.setTitle(title);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
