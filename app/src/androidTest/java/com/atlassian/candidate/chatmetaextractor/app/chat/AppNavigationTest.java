package com.atlassian.candidate.chatmetaextractor.app.chat;

import android.preference.Preference;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.matcher.PreferenceMatchers;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;

import com.atlassian.candidate.chatmetaextractor.app.BuildConfig;
import com.atlassian.candidate.chatmetaextractor.app.ChatApplication;
import com.atlassian.candidate.chatmetaextractor.app.R;

import org.hamcrest.Matcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.contrib.DrawerActions.open;
import static android.support.test.espresso.contrib.DrawerMatchers.isClosed;
import static android.support.test.espresso.contrib.NavigationViewActions.navigateTo;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

/**
 * Tests for the {@link DrawerLayout} layout component in {@link ChatActivity} which manages
 * navigation within the app.
 *
 * @author Gennady Denisov
 */
@LargeTest
@RunWith(AndroidJUnit4.class)
public class AppNavigationTest {


    @Rule
    public ActivityTestRule<ChatActivity> mChatActivityTestRule =
            new ActivityTestRule<ChatActivity>(ChatActivity.class) {

                /**
                 * Cleanup chat before launching activity.
                 */
                @Override
                protected void beforeActivityLaunched() {
                    super.beforeActivityLaunched();
                    // Doing this in @Before generates a race condition.
                    ((ChatApplication) InstrumentationRegistry.getTargetContext()
                            .getApplicationContext()).getChatRepositoryComponent()
                            .getChatRepository().deleteAll();
                }
            };


    @Test
    public void clickOnAboutNavigationItem_ShowsAboutScreen() throws Exception {
        // Open Drawer to click on navigation.
        onView(withId(R.id.drawer_layout))
                .check(matches(isClosed(Gravity.START))) // Left Drawer should be closed.
                .perform(open()); // Open Drawer

        // Start about screen.
        onView(withId(R.id.nav_view))
                .perform(navigateTo(R.id.about_menu_item));

        onData(PreferenceMatchers.withSummaryText(BuildConfig.VERSION_NAME)).check(matches(isDisplayed()));

        Matcher<Preference> mobilizationPref = PreferenceMatchers.withKey(
                InstrumentationRegistry.getTargetContext().getString(R.string.pref_key_source));

        onData(mobilizationPref).check(matches(isDisplayed()));
    }
}