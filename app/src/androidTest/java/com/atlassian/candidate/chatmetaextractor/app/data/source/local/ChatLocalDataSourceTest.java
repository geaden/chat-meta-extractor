package com.atlassian.candidate.chatmetaextractor.app.data.source.local;

import android.content.ContentResolver;
import android.database.Cursor;
import android.support.test.InstrumentationRegistry;
import android.support.test.filters.LargeTest;
import android.support.test.runner.AndroidJUnit4;

import com.atlassian.candidate.chatmetaextractor.app.data.ChatMessage;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

/**
 * Integration tests for {@link ChatLocalDataSource} implementation.
 *
 * @author Gennady Denisov
 */
@RunWith(AndroidJUnit4.class)
@LargeTest
public class ChatLocalDataSourceTest {

    private ContentResolver mContentResolver;
    private ChatLocalDataSource mLocalDataSource;

    @Before
    public void setup() {
        mContentResolver = InstrumentationRegistry.getTargetContext().getContentResolver();
        mLocalDataSource = ChatLocalDataSource.getInstance(mContentResolver);
    }

    @After
    public void cleanUp() {
        mLocalDataSource.deleteAll();
    }

    @Test
    public void testPreconditions() {
        assertNotNull(mLocalDataSource);
    }

    @Test
    public void saveChatMessage_retrieveChatMessage() {
        // Given a message
        final ChatMessage message = new ChatMessage();
        message.setBody("{\"mentions\": [\"bob\"], \"emoticons\": [\"sunny\"], \"links\": " +
                "[{\"title\": \"foobar\", \"url\": \"http://foo.bar\"}, " +
                "{\"title\": \"barfoo\", \"url\": \"http://bar.foo\"}]}");
        message.setMessage("@bob (sunny) http://foo.bar");
        // When saved into persistent repository
        mLocalDataSource.saveMessage(message);
        // Then the message can be retrieved from the persisted repository
        Cursor chatCursor = mContentResolver.query(
                ChatPersistenceContract.ChatEntry.buildChatUriWithId(message.getId()),
                ChatPersistenceContract.ChatEntry.CHAT_COLUMNS,
                null,
                new String[]{String.valueOf(message.getId())},
                null
        );
        chatCursor.moveToFirst();
        final ChatMessage savedMessage = ChatMessage.from(chatCursor);
        assertThat(savedMessage.getBody(), is(equalTo(message.getBody())));
        assertThat(savedMessage.getMessage(), is(equalTo(message.getMessage())));
        assertThat(savedMessage.getId(), is(equalTo(message.getId())));
        assertThat(savedMessage.getCreated(), is(notNullValue()));
        // Free up cursor
        chatCursor.close();
    }
}