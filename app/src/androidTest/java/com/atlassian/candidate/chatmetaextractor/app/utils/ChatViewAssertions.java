package com.atlassian.candidate.chatmetaextractor.app.utils;

import android.support.test.espresso.NoMatchingViewException;
import android.support.test.espresso.ViewAssertion;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Custom view assertions to be used during tests. .
 *
 * @author Gennady Denisov
 */
public class ChatViewAssertions {
    public static ViewAssertion hasItemsCount(final int count) {
        return new ViewAssertion() {
            @Override
            public void check(View view, NoMatchingViewException e) {
                if (!(view instanceof RecyclerView)) {
                    throw e;
                }
                RecyclerView rv = (RecyclerView) view;
                assertThat(rv.getAdapter().getItemCount(), is(count));
            }
        };
    }

    public static ViewAssertion containsText(final String partialText) {
        return new ViewAssertion() {
            @Override
            public void check(final View view, final NoMatchingViewException e) {
                if (!(view instanceof TextView)) {
                    throw e;
                }
                TextView tv = (TextView) view;
                assertThat(tv.getText().toString(), containsString(partialText));
            }
        };
    }
}
