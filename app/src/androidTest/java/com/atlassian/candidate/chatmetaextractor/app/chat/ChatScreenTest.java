package com.atlassian.candidate.chatmetaextractor.app.chat;

import android.support.test.InstrumentationRegistry;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.atlassian.candidate.chatmetaextractor.app.ChatApplication;
import com.atlassian.candidate.chatmetaextractor.app.R;
import com.atlassian.candidate.chatmetaextractor.app.utils.ChatMatchers;
import com.atlassian.candidate.chatmetaextractor.app.utils.ChatViewAssertions;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

/**
 * Tests for the main screen with chat.
 *
 * @author Gennady Denisov
 */
@RunWith(AndroidJUnit4.class)
@LargeTest
public class ChatScreenTest {

    private static final String TEST_MESSAGE = "@bob (yay) http://www.foo.com";

    @Rule
    public ActivityTestRule<ChatActivity> mChatActivityTestRule =
            new ActivityTestRule<ChatActivity>(ChatActivity.class) {

                /**
                 * Cleanup chat before launching activity.
                 */
                @Override
                protected void beforeActivityLaunched() {
                    super.beforeActivityLaunched();
                    // Doing this in @Before generates a race condition.
                    ((ChatApplication) InstrumentationRegistry.getTargetContext()
                            .getApplicationContext()).getChatRepositoryComponent()
                            .getChatRepository().deleteAll();
                }
            };

    @Test
    public void sendMessage() throws Exception {
        // When enter message
        onView(withId(R.id.message)).perform(typeText(TEST_MESSAGE), closeSoftKeyboard());
        // and send button clicked
        onView(withId(R.id.send)).perform(click());

        // Check new message added
        onView(withId(R.id.chat)).check(ChatViewAssertions.hasItemsCount(1));
        // Verify message is displayed on a screen
        onView(ChatMatchers.withItemText(TEST_MESSAGE)).check(matches(isDisplayed()));

    }
}
