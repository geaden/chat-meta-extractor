package com.atlassian.candidate.chatmetaextractor.app.chat;

import com.atlassian.candidate.chatmetaextractor.app.data.ChatMessage;

import org.mockito.ArgumentMatcher;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.argThat;

/**
 * Custom matcher for mockito to match a {@link ChatMessage}.
 *
 * @author Gennady Denisov
 */
public class MessageArgumentMatcher extends ArgumentMatcher<ChatMessage> {
    private ChatMessage expected;

    public static ChatMessage eq(final ChatMessage expected) {
        return argThat(new MessageArgumentMatcher(expected));
    }

    private MessageArgumentMatcher(final ChatMessage expected) {
        this.expected = expected;
    }

    @Override
    public boolean matches(final Object argument) {
        final ChatMessage actual = (ChatMessage) argument;
        assertThat(actual.getMessage(), is(equalTo(expected.getMessage())));
        assertThat(actual.getBody(), is(equalTo(expected.getBody())));
        return true;
    }
}
