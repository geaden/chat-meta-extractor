package com.atlassian.candidate.chatmetaextractor.app.chatdetail;

import android.database.Cursor;
import android.net.Uri;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.util.Log;

import com.atlassian.candidate.chatmetaextractor.app.data.source.LoaderProvider;
import com.atlassian.candidate.chatmetaextractor.app.data.source.local.ChatPersistenceContract;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.Date;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Unit tests for {@link ChatDetailPresenter}.
 *
 * @author Gennady Denisov
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(Log.class)
public class ChatDetailPresenterTest {

    private static final String TEST_BODY = "{}";
    private static final String TEST_CREATED_AT = "2017-03-08 00:00:00";

    @Mock
    private ChatDetailContract.View mView;

    @Mock
    private LoaderManager mLoaderManager;

    @Mock
    private LoaderProvider mLoaderProvider;

    @Mock
    private Uri mChatUri;

    private ChatDetailPresenter mChatDetailPresenter;

    @Mock
    private Cursor mChatCursor;


    @Before
    public void setupChatPresenter() {
        MockitoAnnotations.initMocks(this);
        PowerMockito.mockStatic(Log.class);
        mChatDetailPresenter = new ChatDetailPresenter(mChatUri, mLoaderProvider, mLoaderManager, mView);
        verify(mView).setPresenter(mChatDetailPresenter);
    }

    @Test
    public void loadChatIntoView() {
        // When data loaded
        stubCursor();
        //noinspection unchecked
        mChatDetailPresenter.onLoadFinished(mock(Loader.class), mChatCursor);

        // Then loading indicator is hidden and message body is displayed
        verify(mView).showBody("{}");
        verify(mView).showTitle(any(Date.class));
        verify(mView).setLoadingIndicator(false);
    }

    @Test
    public void loadChat_EmptyData() {
        // When cursor returned empty data
        //noinspection unchecked
        mChatDetailPresenter.onLoadFinished(mock(Loader.class), mChatCursor);

        // Then loading indicator is hidden and body and title are hidden
        verify(mView).setLoadingIndicator(false);
        verify(mView).hideBody();
        verify(mView).hideCreatedAt();
    }

    @Test
    public void shareChat() {
        // When data loaded
        stubCursor();
        //noinspection unchecked
        mChatDetailPresenter.onLoadFinished(mock(Loader.class), mChatCursor);
        // and share data clicked
        mChatDetailPresenter.shareChat();

        // Then share options are shown
        verify(mView).openShareOptions(TEST_BODY);
    }

    private void stubCursor() {
        when(mChatCursor.moveToLast()).thenReturn(true);
        when(mChatCursor.getColumnIndex(ChatPersistenceContract.ChatEntry.COLUMN_NAME_BODY))
                .thenReturn(2);
        when(mChatCursor.getColumnIndex(ChatPersistenceContract.ChatEntry.COLUMN_NAME_CREATED_AT))
                .thenReturn(3);
        when(mChatCursor.getString(2)).thenReturn(TEST_BODY);
        when(mChatCursor.getString(3)).thenReturn(TEST_CREATED_AT);
    }
}
