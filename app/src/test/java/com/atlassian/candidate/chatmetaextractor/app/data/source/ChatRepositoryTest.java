package com.atlassian.candidate.chatmetaextractor.app.data.source;

import com.atlassian.candidate.chatmetaextractor.app.data.ChatMessage;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.verify;

/**
 * Unit-tests for the implementation of local chat repository.
 *
 * @author Gennady Denisov
 */
public class ChatRepositoryTest {

    @Mock
    private ChatDataSource mChatLocalDataSource;

    private ChatRepository mChatRepository;

    private ArgumentCaptor<Long> messageIdArgumentCaptor = ArgumentCaptor.forClass(Long.class);

    @Before
    public void setupChatRepository() {
        MockitoAnnotations.initMocks(this);

        // Instantiate class under test
        mChatRepository = new ChatRepository(mChatLocalDataSource);
    }

    @After
    public void cleanup() {
        mChatRepository = null;
    }

    @Test
    public void saveMessage() {
        // Given a stub message
        final ChatMessage message = new ChatMessage();
        message.setBody("{}");
        message.setMessage("foo bar");

        // When save to the repository
        mChatRepository.saveMessage(message);

        // Then persistent data source is called
        verify(mChatLocalDataSource).saveMessage(message);
    }

    @Test
    public void deleteMessage() {
        // When delete message from repository
        mChatRepository.deleteMessage(1L);

        // Then message deleted from persistent storage
        verify(mChatLocalDataSource).deleteMessage(messageIdArgumentCaptor.capture());
        assertThat(messageIdArgumentCaptor.getValue(), is(equalTo(1L)));
    }

    @Test
    public void deleteAll() {
        // When delete all messages from repository
        mChatRepository.deleteAll();

        // Then messages are deleted from persistent storage
        verify(mChatLocalDataSource).deleteAll();
    }

}