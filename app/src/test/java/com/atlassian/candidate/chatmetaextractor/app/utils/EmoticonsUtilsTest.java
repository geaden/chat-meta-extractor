package com.atlassian.candidate.chatmetaextractor.app.utils;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ImageSpan;
import android.widget.TextView;

import com.bumptech.glide.BitmapRequestBuilder;
import com.bumptech.glide.BitmapTypeRequest;
import com.bumptech.glide.DrawableTypeRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Unit tests for {@link EmoticonsUtils}.
 *
 * @author Gennady Denisov
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({Glide.class, EmoticonsUtils.class, SpannableStringBuilder.class,
        BitmapDrawable.class, EmoticonSpanTarget.class})
public class EmoticonsUtilsTest {

    @Mock
    private TextView textView;

    @Mock
    private Context context;

    @Mock
    private AssetManager assetManager;

    @Mock
    private SpannableStringBuilder builder;

    @Mock
    private BitmapRequestBuilder<String, Bitmap> brb;

    @Mock
    private BitmapDrawable bd;

    @Mock
    private Bitmap bitmap;

    @Captor
    private ArgumentCaptor<EmoticonSpanTarget> emoticonSpanTarget;

    @Before
    public void setupContext() throws Exception {
        MockitoAnnotations.initMocks(this);
        PowerMockito.mockStatic(Glide.class);
        PowerMockito.whenNew(SpannableStringBuilder.class).withAnyArguments().thenReturn(builder);
        PowerMockito.whenNew(BitmapDrawable.class).withAnyArguments().thenReturn(bd);
        when(textView.getContext()).thenReturn(context);
        when(textView.getLineHeight()).thenReturn(1);
        when(bitmap.getHeight()).thenReturn(1);
        when(bitmap.getWidth()).thenReturn(1);
        when(context.getAssets()).thenReturn(assetManager);
        when(assetManager.open("emoticons.json"))
                .thenReturn(EmoticonsUtilsTest.class.getClassLoader().getResourceAsStream("emoticons.json"));
    }

    @Test
    public void getSpannableText_WithoutEmoticons() throws Exception {
        EmoticonsUtils.setSpannable(textView, "@bob How r u?");
        PowerMockito.verifyNew(SpannableStringBuilder.class).withArguments("@bob How r u?");
        verify(textView).setText(builder, TextView.BufferType.SPANNABLE);
    }

    @SuppressWarnings("unchecked")
    @Test
    public void getSpannableText_WithEmoticons() throws Exception {
        mockGlide();
        EmoticonsUtils.setSpannable(textView, "@bob (foo) (bar)");
        PowerMockito.verifyNew(SpannableStringBuilder.class).withArguments("@bob (foo) (bar)");
        verify(brb, times(2)).into(emoticonSpanTarget.capture());
        emoticonSpanTarget.getValue().onResourceReady(bitmap, mock(GlideAnimation.class));
        verify(builder).setSpan(any(ImageSpan.class), eq(11), eq(16), eq(Spannable.SPAN_EXCLUSIVE_EXCLUSIVE));
        verify(textView, times(2)).setText(builder, TextView.BufferType.SPANNABLE);
    }

    @SuppressWarnings("unchecked")
    @Test
    public void getSpannableText_OnlyEmoticons() {
        mockGlide();
        EmoticonsUtils.setSpannable(textView, "(foo) (bar)");
        verify(brb, times(2)).into(emoticonSpanTarget.capture());
        emoticonSpanTarget.getValue().onResourceReady(bitmap, mock(GlideAnimation.class));
        verify(builder).setSpan(any(ImageSpan.class), eq(6), eq(11), eq(Spannable.SPAN_EXCLUSIVE_EXCLUSIVE));
        verify(textView, times(2)).setText(builder, TextView.BufferType.SPANNABLE);
    }

    @Test
    public void getSpannableText_NotExistentEmoticon() throws Exception {
        EmoticonsUtils.setSpannable(textView, "(yay)");
        PowerMockito.verifyNew(SpannableStringBuilder.class).withArguments("(yay)");
        verify(textView).setText(builder, TextView.BufferType.SPANNABLE);
    }

    @SuppressWarnings("unchecked")
    private void mockGlide() {
        final RequestManager rm = mock(RequestManager.class);
        final DrawableTypeRequest<String> dtr = mock(DrawableTypeRequest.class);
        final BitmapTypeRequest<String> btr = mock(BitmapTypeRequest.class);
        when(rm.load(anyString())).thenReturn(dtr);
        when(dtr.asBitmap()).thenReturn(btr);
        when(btr.diskCacheStrategy(DiskCacheStrategy.ALL)).thenReturn(brb);
        when(brb.centerCrop()).thenReturn(brb);
        when(brb.dontAnimate()).thenReturn(brb);
        when(Glide.with(any(Context.class))).thenReturn(rm);
    }

}