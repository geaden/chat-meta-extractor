package com.atlassian.candidate.chatmetaextractor.app.chat;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;

import com.atlassian.candidate.chatmetaextractor.app.data.ChatMessage;
import com.atlassian.candidate.chatmetaextractor.app.data.source.ChatDataSource;
import com.atlassian.candidate.chatmetaextractor.app.data.source.ChatRepository;
import com.atlassian.candidate.chatmetaextractor.app.data.source.LoaderProvider;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Unit tests for the implementation of {@link ChatPresenter}.
 *
 * @author Gennady Denisov
 */
public class ChatPresenterTest {

    @Mock
    private LoaderProvider mLoaderProvider;

    @Mock
    private ChatRepository mChatRepository;

    @Mock
    private LoaderManager mLoaderManager;

    @Mock
    private ChatContract.View mChatView;

    @Captor
    private ArgumentCaptor<ChatDataSource.ChatCallback> mChatCallbackArgumentCaptor;

    @Captor
    private ArgumentCaptor<ChatExtractCallback> mChatExtractCallbackArgumentCaptor;

    private ChatPresenter mChatPresenter;

    @Mock
    private Cursor mChatCursor;

    @Before
    public void setupChatPresenter() {
        MockitoAnnotations.initMocks(this);

        // Instantiate class under test
        mChatPresenter = new ChatPresenter(mLoaderProvider, mLoaderManager, mChatView, mChatRepository);
        mChatPresenter.setupListeners();
        verify(mChatView).setPresenter(mChatPresenter);
    }

    @Test
    public void loadDataFromRepositoryInitLoader() {
        // When load chat messages
        mChatPresenter.loadMessages();

        // The the loader initializes
        verify(mChatRepository).loadChat(mChatCallbackArgumentCaptor.capture());

        verify(mChatView).setLoadingIndicator(true);
        verify(mChatRepository).loadChat(mChatCallbackArgumentCaptor.capture());
        mChatCallbackArgumentCaptor.getValue().onDataLoaded();

        verify(mLoaderManager).initLoader(anyInt(), any(Bundle.class),
                any(LoaderManager.LoaderCallbacks.class));
    }

    @Test
    public void loadConversationsFromRepositoryAndLoadIntoView() {
        // When cursor has items
        when(mChatCursor.moveToLast()).thenReturn(true);

        // and loading chat messages
        //noinspection unchecked
        mChatPresenter.onLoadFinished(mock(Loader.class), mChatCursor);

        // Then loading indicator is hidden and messages are shown
        verify(mChatView).setLoadingIndicator(false);
        verify(mChatView).showMessages(mChatCursor);
    }

    @Test
    public void loaConversationsShowsEmptyMessage() {
        // When loading chat messages
        mChatPresenter.onLoadFinished(mock(Loader.class), mChatCursor);

        // Then loading indicator is hidden and empty message shown
        verify(mChatView).setLoadingIndicator(false);
        verify(mChatView).showNoMessages();
    }

    @Test
    public void clickOnMessage_ShowsDetailsUi() {
        // Given a stubbed message
        final ChatMessage message = new ChatMessage();
        message.setId(1L);
        message.setBody("foo");
        message.setMessage("bar");

        // When open message details is invoked
        mChatPresenter.openMessageDetails(message);

        // Then message detail UI is shown
        mChatView.showMessageDetailsUi(message);
    }

    @Test
    public void clickOnSendMessage_ExtractsAndSavesMessage() {
        // Given a stubbed message
        final ChatMessage message = new ChatMessage();
        message.setMessage("bar");

        // When send message
        mChatPresenter.sendMessage(message);

        // Then extracting initialized
        verify(mChatView).showExtracting(true);
        verify(mChatView).startExtractService(MessageArgumentMatcher.eq(message),
                mChatExtractCallbackArgumentCaptor.capture());

        // When data extracted
        mChatExtractCallbackArgumentCaptor.getValue().onExtracted("foo");

        // Then message is saved
        verify(mChatView).showExtracting(false);

        assertThat(message.getBody(), is(equalTo("foo")));

        verify(mChatRepository).saveMessage(message);
    }

}