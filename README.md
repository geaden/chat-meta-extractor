# Chat Meta Extractor

Sample Android App to extract meta information from a chat message.

- This is not a real chat app, but a "chat-alike" app.

- It doesn't have a backend, so every message is stored locally only (however it has a potential to use a backend).

- It doesn't autocomplete emoticons during message typing, however it does show emoticons after message was "sent"

## Demo

This is a short video demonstrating app basic functionality

https://youtu.be/_qBAj4OrMPk

## Download

Please, check out download section of this repo to download latest release.

Gennady Denisov, 2017
